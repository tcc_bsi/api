<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nickname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type_user')->nullable();
            $table->integer('verification')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('first_access')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
