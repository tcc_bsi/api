<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('matricula')->nullable();
            $table->string('date_birth')->nullable();
            $table->string('sexo');
            $table->string('CPF');
            $table->string('RG')->nullable();
            $table->string('phone')->nullable();
            $table->string('image')->nullable();
            $table->bigInteger('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('address');
            $table->bigInteger('responsible_id')->unsigned()->nullable();
            $table->foreign('responsible_id')->references('id')->on('responsible');
            $table->bigInteger('class_id')->unsigned()->nullable();
            $table->foreign('class_id')->references('id')->on('class');
            $table->bigInteger('bank_id')->unsigned()->nullable();
            $table->foreign('bank_id')->references('id')->on('bank');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }

}
