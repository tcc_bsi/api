<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>RELATÓRIO</title>


<style>
    .title{
        background-color: #E7E8E7;
        font-size:16pt;
        
        padding:10px;
        text-align: center;
    }
    .content{
        padding-top: 50px !important;
        padding:10px;
        font-size: 20px;
        line-height: 1.5;
        text-align:justify;
    }
    .date{
        font-size: 20px;
        line-height: 1.5;
        text-align:right;
        padding:10px;
        padding-top:100px !important;    
    }
    .signature{
        font-size: 20px;
        line-height: 1.5;
        text-align: center;
        padding-top:200px;
        
    }
    .page{
        page-break-inside: avoid;
    }
</style>

</head>
<body class="plano">
   <div>
       {{-- {{$event}} --}}

        @if ($activity->athletes != '[]')
        @foreach ($activity->athletes as $athlete)
        @if ($athlete->permissao)
            @if ($athlete->student_id->responsible_id != null)
                <div class="page">
                    <div class="title">
                        <b>AUTORIZAÇÃO DE VIAGEM ESCOLAR PARA AS ATIVIDADES ESPORTISTAS (MENORES DE IDADE)</b>
                    </div>
                    <div class="content">
                                Eu,_____________________________________________, portador(a) do RG____________________, e CPF______________________,
                        domiciliado(a) e residente no endereço ____________________________________________________________________ com Nº____________, no bairro____________________________, da cidade de________________________________,
                        autorizo <b>{{$athlete->student_id->first_name.' '.$athlete->student_id->last_name}}</b>, a empreender viagem nacional com destino à <b>{{$activity->event_id->city}}</b> na companhia
                        de <b>{{$valueRequest->responsible->primary_name}}</b>, cujo CPF é: <b>{{$valueRequest->responsible->primary_CPF}}</b> @if ($valueRequest->responsible->second_CPF != '')
                            e <b>{{$valueRequest->responsible->second_name}}</b>, cujo CPF é: <b>{{$valueRequest->responsible->second_CPF}}</b>
                        @endif,
                        consoante ao que estabelece a Lei Federal Nº 8.069/90, art. 83, § 1º, letra “b”, 2. A presente autorização
                        tem a validade desde o dia {{$valueRequest->first_date}} até o dia {{$valueRequest->last_date}}.
                    </div>
                    <div class="date">
                        ________________, ______de______________ de_________.
                    </div>
                    <div class="signature">
                        <b>________________________________________</b><br/>
                        Assinatura do Pai, Mãe ou Responsável Legal<br/>
                        (Reconhecida como verdadeira)
                    </div>
                </div>
            @endif
        @endif
        
            
        @endforeach   
    @endif



       
    </div>
</body>
</html>
