<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>RELATÓRIO</title>


<style>
    .tabeleHome{
    /* background-color: blue; */
    width:100%;
    margin-top:20px;
    border: 5px solid #01870A;
    page-break-inside: avoid;
}
.menuTable{
    background-color: #01870A;
    color:white;
    
}
.menuTable div{
     padding-top: 5px;
     padding-bottom: 5px;
     padding-left: 3px;
}
.menuIndex{
    background-color:#026B09;
    color:white;
}
.menuIndex th{
    padding: 20px;
}
.contentTable tr:nth-child(even) {
    background-color: #EFEFEE
}
.contentTable tr:nth-child(odd) {
    background-color: white
}
.contentTable div {
    padding-left: 3px;
    padding-top: 5px;
     padding-bottom: 5px;
     font-size:14px;
}
.title{
    width:100%;
    padding:5px;
    font-size:15pt;
    background-color: #DFDFDE;
    text-align: center
}
</style>

</head>
<body class="plano">
    {{-- {{$event}} --}}
    <div>
        <div class="title">
            <b>LISTA DE ALUNOS PARA CONFIRMAÇÃO DE PRESENÇA DA VIAGEM</b>
        </div>
        <table class="tabeleHome">
            <thead>
                <tr class="menuIndex">
                        <th colspan="5">
                            <div>
                                <div>
                                    <b style="margin-right:8px;color:#F0D902 !important">VIAGEM PARA O EVENTO:</b>{{$event->title}}
                                </div>
                                <div>
                                    <b style="margin-right:8px;color:#F0D902 !important">DATA DE INÍCIO:</b>{{$event->date_start}}
                                </div>
                                <div>
                                    <b style="margin-right:8px;color:#F0D902 !important">DATA DE TÉRMINO:</b>{{$event->date_end}}
                                </div>
                                <div>
                                    <b style="margin-right:8px;color:#F0D902 !important">CIDADE DESTINO:</b>{{$event->city}}
                                </div>
                            </div>
                        </th>
                        
                    </tr>
                <tr class="menuTable">
                    <th><div>Nome</div></th>
                    <th><div>CPF</div></th>
                    <th><div>RG</div></th>
                    <th><div>Turma/Curso</div></th>
                    <th><div>PRESENÇA</div></th>
                </tr>
            </thead>
            <tbody class="contentTable">
                @foreach ($event->activities as $activity)
                    @if ($activity->athletes != '[]')
                        @foreach ($activity->athletes as $athlete)
                            @if ($athlete->permissao)
                                <tr >
                                    <td><div>{{$athlete->student_id->first_name.' '.$athlete->student_id->last_name}}</div></td>
                                    <td><div>{{$athlete->student_id->CPF}}</div></td>
                                    <td><div>{{$athlete->student_id->RG}}</div></td>
                                    @if ($athlete->student_id->class_id != null)
                                        <td><div><b>{{$athlete->student_id->class_id->title}}</b>{{' -  '}}{{$athlete->student_id->class_id->course_id->title}}</div></td>
                                    @else
                                        <td><div>Não tem turma cadastrada</div></td>
                                    @endif
                                    <td><div></div></td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </tbody>
        </table>  
    </div>
</body>
</html>
