<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EMAIL</title>

    <style>
        .title{
            background-color: #07A968;
            color:white;
            padding:10px;
            text-align: center;
            font-size:20px;
        }
        .content{
            background-color: #F4F3F3;
            color:#1A1919;
            padding:30px;
            text-align: left;
            font-size:19px;
            min-height: 200px;
        }
        .footer{
            background-color: #03623C;
            color:white;
            padding:10px;
            text-align: center;
            font-size:15px;
        }
    </style>
</head>
<body>
    <div class="title">NOME DO SITE</div>
    <div class="content">@yield('content')
    </div>
    <div class="footer">© TCC Euzimar - Turma BSI2016 - IFNMG Campus Arinos MG</div>
</body>
</html>