@extends('mail.layouts.MainLayout')

@section('content')
<div>
    <div style="width:100%;margin-bottom:20px">
        Olá {{$contentMail['recipient']}},
    </div>
    <div style="width:100%;margin-bottom:20px">
        {{$contentMail['content']}}
    </div>
    <div style="width:100%;margin-bottom:20px;font-size:17px">
        Email Recebido de {{$contentMail['sender']}}
    </div>
</div>
@endsection