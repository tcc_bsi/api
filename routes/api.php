<?php

use Illuminate\Http\Request;

//Rotas para a manutenção das demais rotas para a transferência de CORS
Route::group(['middleware' => 'cors'], function(){
    Route::post('/login', 'Api\AuthController@login');
    // Route::post('/register', 'Api\AuthController@register');

    //Rota para a proteção das demais rotas a partir da autenticação do usuário
    Route::middleware('auth:api')->group(function () {
        Route::get('/user', 'Api\ProfileController@getProfile');
        
        Route::get('/logout', 'Api\AuthController@logout');

        //Rotas para a manutenção dos usuários
        Route::prefix('/user')->group(function () {
            Route::get('/getUsers', 'Api\AuthController@getUsers');
            Route::post('setUser', 'Api\AuthController@setUser');
            Route::post('updateUser', 'Api\AuthController@updateUser');
            Route::post('updateProfile', 'Api\ProfileController@updateProfile');
            Route::post('updateAdmin', 'Api\AdminController@updateAdmin');
            Route::post('updateManager', 'Api\ManagerController@updateManager');
        });

        //Rotas para a manutenção dos Estudantes
        Route::prefix('/student')->group(function () {
            Route::post('setStudent', 'Api\StudentController@setStudent');
            Route::get('getStudents', 'Api\StudentController@getStudents');
            Route::get('getStudentsByActivity/{ID}', 'Api\StudentController@getStudentsByActivity');
            Route::get('getStudent/{ID}', 'Api\StudentController@getStudent');
            Route::post('updateStudent', 'Api\StudentController@updateStudent');
            Route::get('deleteStudent/{ID}', 'Api\StudentController@deleteStudent');
        });

        //Rotas para a manutenção das Classes e Cursos
        Route::prefix('/classCourse')->group(function () {
            Route::post('setCourse', 'Api\ClassCourseController@setCourse');
            Route::post('setClass', 'Api\ClassCourseController@setClass');
            Route::get('getCourses', 'Api\ClassCourseController@getCourses');
            Route::get('getAllClass', 'Api\ClassCourseController@getAllClass');
            Route::get('getClassCourses', 'Api\ClassCourseController@getClassCourses');
            Route::get('getClass/{ID}', 'Api\ClassCourseController@getClass');
            Route::post('updateCourse', 'Api\ClassCourseController@updateCourse');
            Route::post('updateClass', 'Api\ClassCourseController@updateClass');
            Route::get('deleteClass/{ID}', 'Api\ClassCourseController@deleteClass');
            Route::get('deleteCourse/{ID}', 'Api\ClassCourseController@deleteCourse');
        });

        //Rotas para a manutenção das Modalidades
        Route::prefix('/modality')->group(function () {
            Route::post('setModality', 'Api\ModalityController@setModality');
            Route::get('getModalitys', 'Api\ModalityController@getModalitys');
            Route::get('getModality/{ID}', 'Api\ModalityController@getModality');
            Route::post('updateModality', 'Api\ModalityController@updateModality');
            Route::get('deleteModality/{ID}', 'Api\ModalityController@deleteModality');
            Route::get('modality_img/{ID}', 'Api\ModalityController@getImg');
        });

        //Rotas para a manutenção dos Documentos dos estudantes
        Route::prefix('/docs')->group(function () {
            Route::post('setDocuments', 'Api\DocumentsController@setDocuments');
            Route::get('getDocuments/{ID}', 'Api\DocumentsController@getDocuments');
            Route::get('getDocument/{ID}', 'Api\DocumentsController@getDocument');
            Route::post('updateDocuments', 'Api\DocumentsController@updateDocument');
            Route::get('deleteDocument/{ID}', 'Api\DocumentsController@deleteDocument');
            Route::get('downloadDocument/{ID}', 'Api\DocumentsController@getDocumentDownload');
        });

        //Rotas para a manutenção dos Eventos esportistas
        Route::prefix('/events')->group(function () {
            Route::post('setEvent', 'Api\EventController@setEvent');
            Route::get('getEvent/{ID}', 'Api\EventController@getEvent');
            Route::get('getEvents', 'Api\EventController@getEvents');
            Route::post('updateEvent', 'Api\EventController@updateEvent');
            Route::get('deleteEvent/{ID}', 'Api\EventController@deleteEvent');
        });

        //Rotas para a manutenção das Atividades esportistas
        Route::prefix('/activities')->group(function () {
            Route::post('setActivity', 'Api\ActivityController@setActivity');
            Route::get('getActivity/{ID}', 'Api\ActivityController@getActivity');
            Route::get('getActivities/{ID}', 'Api\ActivityController@getActivities');
            Route::post('updateActivity', 'Api\ActivityController@updateActivity');
            Route::get('deleteActivity/{ID}', 'Api\ActivityController@deleteActivity');
        });

        //Rotas para a manutenção dos atletas de uma modalidade/Atividade
        Route::prefix('/athletes')->group(function () {
            Route::post('setAthletes', 'Api\AthletesController@setAthletes');
            // Route::get('getAllAthletes', 'Api\AthletesController@getAllAthletes');
            Route::get('getAthletes/{ID}', 'Api\AthletesController@getAthletes');
            Route::get('deleteAthlete/{ID}', 'Api\AthletesController@deleteAthlete');
        });

        //Rotas para a manutenção dos managers(Professores)
        Route::prefix('/managers')->group(function () {
            Route::post('setManager', 'Api\ManagerController@setManager');
            Route::get('getManager/{ID}', 'Api\ManagerController@getManager');
            Route::post('updateManager', 'Api\ManagerController@updateManager');
        });

        //Rotas para gerenciamento dos relatórios
        Route::prefix('/reports')->group(function () {
            Route::get('getReportGeneral/{ID}', 'Api\ReportController@getReportGeneral');
            Route::get('getReportPassagers/{ID}', 'Api\ReportController@getReportPassengers');
            Route::post('getReportAutorization/{ID}', 'Api\ReportController@getReportAutorization');
            Route::get('getReportActivityAthletes/{ID}', 'Api\ReportController@getReportActivityAthletes');
            Route::post('getReportActivityAutorization/{ID}', 'Api\ReportController@getReportActivityAutorization');
            Route::get('getReportActivityPassagers/{ID}', 'Api\ReportController@getReportActivityPassengers');
        });

        //Rota para envio de email
        Route::prefix('/email')->group(function () {
            Route::post('/sendEmail', 'Api\MailController@sendMail');
        });

        //Rotas para a manutenção do CEP indicado na interface no momento do cadastro de endereço
        Route::get("/cep/{CEP}", function($CEP, Canducci\Cep\Contracts\ICep $c)
        {    
            $cep = $c->find($CEP);
            return $cep->toJson()->result();
        });
    });    
}); 

