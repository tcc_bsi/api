<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{ID}', 'Api\ReportController@getReportGeneral')->name('reportEventStudentActivity');

// Route::get('/{ID}', 'Api\ReportController@getReportPassengers')->name('reportEventStudentActivity');



Route::get('/{ID}', 'Api\ReportController@getReportAutorizationActivity')->name('getReportActivityAthletes');
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'Api\MailController@sendMail');