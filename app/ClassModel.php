<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassModel extends Model
{
    // Tabela usada nesta model 

    protected $table = 'class';

    // Informações da tabela 'class' liberados para inserção

    protected $fillable = [
        'title', 'course_id'
    ];

    // Relacionamento 1x1 entre as tabelas 'class' e 'Course' 

    public function course()
    {
        return $this->belongsTo('App\Course','course_id');
    }

}
