<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Athletes extends Model
{
    // Tabela usada nesta model

    protected $table = 'athletes';

    // Informações da tabela 'activities' liberados para inserção

    protected $fillable = [
        'student_id', 'activity_id'
    ];

    public function student()
    {
        return $this->hasMany('App\Student','student_id');
    }
}
