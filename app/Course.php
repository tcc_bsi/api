<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    // Tabela usada nesta model 

    protected $table = 'course';

    // Informações da tabela 'course' liberados para inserção

    protected $fillable = [
        'title', 'image'
    ];

    // Relacionamento 1x1 entre as tabelas 'course' e 'class' 
    public function classModel()
    {
        return $this->hasMany('App\ClassModel','course_id');
    }

}
