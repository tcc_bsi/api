<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holder extends Model
{
    // Tabela usada nesta model

    protected $table = 'holder';

    // Informações da tabela 'holder' liberados para inserção

    protected $fillable = [
        'student_id', 'user_id'
    ];
}
