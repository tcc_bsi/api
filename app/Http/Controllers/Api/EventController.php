<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Image;
use App\Events;
use App\Log;

class EventController extends Controller
{
    //CADASTRAR UM NOVO EVENTO
    public function setEvent(Request $request)
    {
        $data = $request->all();
        $event = json_decode($data['value'], true);
        if ($request->file('img') != null) {
            $path = $request->file('img')->store('eventImage', 'local');
        } else {
            $path = null;
        }
        $event['image'] = $path;
        $event = Events::create($event);

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a o cadastro do evento cujo ID é: '.$event['id']
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true,
            'data' => $event
        ]);
    }

    //ALTERAR UM EVENTO CADASTRADO
    public function updateEvent(Request $request)
    {
        $data = $request->all();
        $eventValue = json_decode($data['value'], true);
        $eventBase = Events::find($eventValue['id']);
        if ($request->file('img') != null) {
            if ($eventBase['image'] != "") {
                Storage::disk('local')->delete($eventBase['image']);             //deletar antiga imagem
                $path = $request->file('img')->store('eventImage','local');       //salvando nova imagem
                $eventValue['image'] = $path;
            } else {
                $path = $request->file('img')->store('eventImage','local');       //salvando nova imagem
                $eventValue['image'] = $path;
            }
        } else {
            $eventValue['image'] = $eventBase['image'];
        }
        $eventBase->update($eventValue);
        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a alteração do evento cujo ID é: '.$eventBase['id']
        );
        Log::create($dataLog);
        return $eventBase;
    }

    //EXCLUIR UM EVENTO CADASTRADO
    public function deleteEvent($ID)
    {
       $event = Events::find($ID);
        if ($event['image'] != "") {
            Storage::disk('local')->delete($event['image']);
        }
        $result = $event->delete();
        if ($result) {
            return response()->json([
                'success' => true
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //BUSCAR EVENTOS CADASTRADOS
    public function getEvents()
    {
        $events = Events::all();
        foreach ($events as $value) {
            $value['image'] = $this->getImg($value['id']);
        }
        return response()->json([
            'data' => $events
        ]);
    }

    //BUSCAR EVENTO PELO ID
    public function getEvent($ID)
    {
        $event = Events::find($ID);
        $event['image'] = $this->getImg($ID);

        return response()->json([
            'data' => $event
        ]);
    }

    //BUSCAR IMAGEM DO EVENTO A PARTIR DO ID

    public function getImg($ID)
    {
        $nameImg = Events::find($ID);
        if ( $nameImg['image'] == "") {
            $path = "eventImage/default.jpg";
        } else {
            $path = $nameImg['image'];
        }
        
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}
