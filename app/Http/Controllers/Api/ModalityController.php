<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Modality;
use Illuminate\Support\Facades\Storage;
use Image;

class ModalityController extends Controller
{
    //Cadastrar nova modalidade 

    public function setModality(Request $request)
    {
        $data = $request->all();

        $modality = json_decode($data['value'], true);
        
        if ($request->file('img') != null) {
            
            $path = $request->file('img')->store('modality_img','local');
            
        } else {
            $path = null;
        }
        $modality['image'] = $path;
        $modality = Modality::create($modality);
        return response()->json([
            'success' => true,
            'modality' => $modality
        ]);
    }

    //Buscar imagem da modalidade a partir do nome do arquivo

    public function getImg($ID)
    {
        $nameImg = Modality::find($ID);
        if ( $nameImg['image'] == "") {
            $path = "modality_img/default.jpg";
        } else {
            $path = $nameImg['image'];
        }
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }

    //Buscar todas as modalidade cadastada

    public function getModalitys()
    {
        $data = Modality::all();
        foreach ($data  as $modality) {
            $modality['image'] = $this->getImg($modality['id']);
        }
        return $data;   
    }

    //Buscar modalidade cadastada, pelo ID

    public function getModality($ID)
    {
        $modality = Modality::find($ID);
        $modality['modality_id'] = Modality::find($modality['modality_id']);
        if ($modality['modality_id'] == null) {
            $modality['modality_id'] = 0;
        }
        $modality['image'] = $this->getImg($modality['id']);
        return $modality;   
    }

    //Editar modalidade cadastada, pelo ID

    public function updateModality(Request $request)
    {
        $data = $request->all();
        $modalityValue = json_decode($data['value'], true);
        $modalityBase = Modality::find($modalityValue['id']);
        if ($request->file('img') != null) {
            Storage::disk('local')->delete($modalityBase['image']);             //deletar antiga imagem
            $path = $request->file('img')->store('modality_img','local');       //salvando nova imagem
            $modalityValue['image'] = $path;
        } else {
            $modalityValue['image'] = $modalityBase['image'];
        }
        $modalityBase->update($modalityValue);

        return $modalityBase;
    }


    //Método Recursivo para Excluir uma modalidade e suas subModalidades

    public function deleteModalitysRecurse($n)
    {
        $data = Modality::where('modality_id',$n)->get();
        
        if ($data == "[]") {
            return true;
        } else {    
            foreach ($data as $var) {
                $i = $var->id;
                $var['children'] = $this->deleteModalitysRecurse($i);
                $var = Modality::find($i);
                Storage::disk('local')->delete($var['image']);             //deletar imagem relacionada
                $var->delete();
            }
        }
        return true;
    }

    //Excluir modalidade cadastada, pelo ID

    public function deleteModality($ID)
    {
        $this->deleteModalitysRecurse($ID);
        $var = Modality::find($ID);
        Storage::disk('local')->delete($var['image']);             //deletar imagem relacionada
        $var->delete();
        $result = true;
        if ($result) {
            return response()->json([
                'success' => true
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

}
