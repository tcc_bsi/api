<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Manager;
use Illuminate\Support\Facades\Auth;
use App\Log;

class ManagerController extends Controller
{
    //Cadastrar novo Gerente(Professor)
    public function setManager(Request $request)
    {
        $data = $request->all();
        $manager = json_decode($data['value'], true);
        if ($request->file('img') != null) {
            $path = $request->file('img')->store('userImage', 'local');
        } else {
            $path = null;
        }
        $manager['image'] = $path;
        $manager = Manager::create($manager);
        return response()->json([
            'success' => true,
            'data' => $manager
        ]);
    }

    //Editar um Gerente(Professor) cadastrado
    public function updateManager(Request $request)
    {
        $managerRequest = $request->all();
        $managerData = Manager::find($managerRequest['id']);
        $managerRequest['image'] = $managerData['image'];
        
        $managerData->update($managerRequest);

         $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Alterou seus dados de cadastro '
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true
        ]);
    }

    //Buscar todos os Gerentes(Professores) cadastrados
    public function getManagers()
    {
        # code...
    }

    //Buscar Gerente(Professor) pelo ID
    public function getManager($ID)
    {
        # code...
    }

    //Excluir um Gerente(Professor) cadastrado
    public function deleteManager($ID)
    {
        # code...
    }
}
