<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Events;
use App\Activities;
use App\Athletes;
use App\Student;
use DomPDF;
use App\ClassModel;
use App\Course;
use App\Responsible;
use App\Log;
// use Illuminate\Support\Facades\Storage;
// use Image;

class ReportController extends Controller
{
    public function getReportGeneral($ID)
    {
        $event = Events::find($ID);
        $event['activities'] = Activities::where('event_id', $event['id'])->get();
        foreach ($event['activities'] as $activity) {
            $activity['athletes'] = Athletes::where('activity_id', $activity['id'])->get();
            // $atividades = array('' => , );
            foreach ($activity['athletes'] as $athlete) {
                $athlete->student_id = Student::find($athlete['student_id']);

                $athlete['student_id']['CPF'] = $this->mask_CPF($athlete['student_id']['CPF']);
                   $athlete['student_id']['date_birth'] = $this->mask_Data($athlete['student_id']['date_birth']);
                   $athlete['student_id']['phone'] = $this->mask_phone($athlete['student_id']['phone']);
                   $athlete['student_id']['class_id'] = ClassModel::find($athlete['student_id']['class_id']); 
                   
                   if ($athlete['student_id']['class_id'] != null) {
                    $athlete->student_id->class_id->course_id = Course::find( $athlete->student_id->class_id->course_id);     
                   }
            }
        }
        // setPaper('a4', 'landscape')->

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Gerou o relatório geral do evento cujo id é: '.$event['id']
        );
        Log::create($dataLog);

        $pdf = DomPDF::loadView('report.eventStudentActivity',compact('event'));
        return $pdf->download('documento.pdf');
        // return view('report.eventStudentActivity', compact('event', 'image'));
    }

    // Relatório de alunos pela modalidade selecionada
    public function getReportActivityAthletes($ID)
    {
        // $event = Events::find($ID);
        $activity = Activities::find($ID);
        
        $activity['athletes'] = Athletes::where('activity_id', $activity['id'])->get();
        $activity['event_id'] = Events::where('id', $activity['event_id'])->first();
        // dd($activity['event_id']);
            // $atividades = array('' => , );
            foreach ($activity['athletes'] as $athlete) {
                $athlete->student_id = Student::find($athlete['student_id']);

                $athlete['student_id']['CPF'] = $this->mask_CPF($athlete['student_id']['CPF']);
                   $athlete['student_id']['date_birth'] = $this->mask_Data($athlete['student_id']['date_birth']);
                   $athlete['student_id']['phone'] = $this->mask_phone($athlete['student_id']['phone']);
                   $athlete['student_id']['class_id'] = ClassModel::find($athlete['student_id']['class_id']); 
                   
                   if ($athlete['student_id']['class_id'] != null) {
                    $athlete->student_id->class_id->course_id = Course::find( $athlete->student_id->class_id->course_id);     
                   }
            }
        // setPaper('a4', 'landscape')->

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Gerou o relatório de alunos da atividade cujo id é: '.$activity['id']
        );
        Log::create($dataLog);

        $pdf = DomPDF::loadView('report.activityAtlhetes',compact('activity'));
        return $pdf->download('Documento-Atletas'.'-'.$activity->title.'.pdf');
        // return view('report.activityAtlhetes', compact('activity'));
    }

    //Gerar relatório de autorização para as viagens
    public function getReportAutorization($ID, Request $request)
    {
        $valueRequest = $request->all();
        //  dd($valueRequest);
        $valueRequest['first_date'] = $this->mask_Data($valueRequest['first_date']);
        $valueRequest['last_date'] = $this->mask_Data($valueRequest['last_date']);
        $valueRequest['responsible']['primary_CPF'] = $this->mask_CPF($valueRequest['responsible']['primary_CPF']);
        if ($valueRequest['responsible']['second_CPF'] != '') {
            $valueRequest['responsible']['second_CPF'] = $this->mask_CPF($valueRequest['responsible']['second_CPF']);
        }
        $valueRequest = json_decode(json_encode($valueRequest), 0);
        
        $event = Events::find($ID);
        $verifyCPF = array();

        $event['activities'] = Activities::where('event_id', $event['id'])->get();
        foreach ($event['activities'] as $activity) {
            $activity['athletes'] = Athletes::where('activity_id', $activity['id'])->get();
            foreach ($activity['athletes'] as $athlete) {
                
                $athlete['student_id'] = Student::find($athlete['student_id']);
                // Verificar CPF
                $cont=0;
                foreach ($verifyCPF as $cpf) {
                    if ($cpf == $athlete['student_id']['CPF']) {
                        $cont = $cont+1;
                    }
                }
                if ($cont==0) {
                    array_push($verifyCPF, $athlete['student_id']['CPF']);
                    $athlete['permissao'] = true;
                    
                } else {
                    $athlete['permissao'] = false;
                }
                $athlete['student_id']['CPF'] = $this->mask_CPF($athlete['student_id']['CPF']);
                $athlete['student_id']['date_birth'] = $this->mask_Data($athlete['student_id']['date_birth']);
                $athlete['student_id']['phone'] = $this->mask_phone($athlete['student_id']['phone']);
                $class = ClassModel::find($athlete['student_id']['class_id']); 
                $class['course_id'] = Course::find($class['course_id']);       
                $athlete['student_id']['class_id'] = $class;   
                $athlete['student_id']['responsible_id'] = Responsible::find($athlete['student_id']['responsible_id']);
                
            }
        }

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Gerou o relatório de Autorização dos alunos do evento cujo id é: '.$event['id']
        );
        Log::create($dataLog);

        $pdf = DomPDF::loadView('report.autorization',compact('event', 'valueRequest'));
        return $pdf->download('documento.pdf');
        // return view('report.autorization', compact('event', 'valueRequest'));
    }

    //Gerar relatório de autorização para as viagens
    public function getReportActivityAutorization($ID, Request $request)
    {
        $valueRequest = $request->all();
        // dd($valueRequest);
        $valueRequest['first_date'] = $this->mask_Data($valueRequest['first_date']);
        $valueRequest['last_date'] = $this->mask_Data($valueRequest['last_date']);
        $valueRequest['responsible']['primary_CPF'] = $this->mask_CPF($valueRequest['responsible']['primary_CPF']);
        if ($valueRequest['responsible']['second_CPF'] != '') {
            $valueRequest['responsible']['second_CPF'] = $this->mask_CPF($valueRequest['responsible']['second_CPF']);
        }
        $valueRequest = json_decode(json_encode($valueRequest), 0);
        
        // $event = Events::find($ID);
        $verifyCPF = array();

        $activity = Activities::find($ID);
        $activity['athletes'] = Athletes::where('activity_id', $activity['id'])->get();
        $activity['event_id'] = Events::where('id', $activity['event_id'])->first();
            foreach ($activity['athletes'] as $athlete) {
                
                $athlete['student_id'] = Student::find($athlete['student_id']);
                // Verificar CPF
                $cont=0;
                foreach ($verifyCPF as $cpf) {
                    if ($cpf == $athlete['student_id']['CPF']) {
                        $cont = $cont+1;
                    }
                }
                if ($cont==0) {
                    array_push($verifyCPF, $athlete['student_id']['CPF']);
                    $athlete['permissao'] = true;
                    
                } else {
                    $athlete['permissao'] = false;
                }
                $athlete['student_id']['CPF'] = $this->mask_CPF($athlete['student_id']['CPF']);
                $athlete['student_id']['date_birth'] = $this->mask_Data($athlete['student_id']['date_birth']);
                $athlete['student_id']['phone'] = $this->mask_phone($athlete['student_id']['phone']);
                $class = ClassModel::find($athlete['student_id']['class_id']); 
                $class['course_id'] = Course::find($class['course_id']);       
                $athlete['student_id']['class_id'] = $class;   
                $athlete['student_id']['responsible_id'] = Responsible::find($athlete['student_id']['responsible_id']);
                
            }

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Gerou o relatório de autorização de viagem dos alunos da atividade cujo id é: '.$activity['id']
        );
        Log::create($dataLog);

        $pdf = DomPDF::loadView('report.autorizationActivity',compact('activity', 'valueRequest'));
        return $pdf->download('Documento-Autorização_de_responsavel'.'-'.$activity->title.'.pdf');
        // return view('report.autorizationActivity', compact('activity', 'valueRequest'));
    }

    //Gerar relatório de confirmação de passageiros para motoriastas
    public function getReportPassengers($ID)
    {
        $event = Events::find($ID);
        $verifyCPF = array();

        $event['activities'] = Activities::where('event_id', $event['id'])->get();
        foreach ($event['activities'] as $activity) {
            $activity['athletes'] = Athletes::where('activity_id', $activity['id'])->get();
            foreach ($activity['athletes'] as $athlete) {
                
                $athlete['student_id'] = Student::find($athlete['student_id']);
                // Verificar CPF
                $cont=0;
                foreach ($verifyCPF as $cpf) {
                    if ($cpf == $athlete['student_id']['CPF']) {
                        $cont = $cont+1;
                    }
                }
                if ($cont==0) {
                    array_push($verifyCPF, $athlete['student_id']['CPF']);
                    $athlete['permissao'] = true;
                    
                } else {
                    $athlete['permissao'] = false;
                }
                $athlete['student_id']['CPF'] = $this->mask_CPF($athlete['student_id']['CPF']);
                   $athlete['student_id']['date_birth'] = $this->mask_Data($athlete['student_id']['date_birth']);
                   $athlete['student_id']['phone'] = $this->mask_phone($athlete['student_id']['phone']);
                   $athlete['student_id']['class_id'] = ClassModel::find($athlete['student_id']['class_id']); 
                   
                   if ($athlete['student_id']['class_id'] != null) {
                    $athlete->student_id->class_id->course_id = Course::find( $athlete->student_id->class_id->course_id);     
                   }
                       
                
            }
        }

         $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Gerou o relatório de presença do evento cujo id é: '.$event['id']
        );
        Log::create($dataLog);

        $pdf = DomPDF::loadView('report.passengers',compact('event'));
        return $pdf->download('documento.pdf');
        // return view('report.passengers', compact('event'));
    }

    //Gerar relatório de confirmação de passageiros para motoriastas
    public function getReportActivityPassengers($ID)
    {
        // $event = Events::find($ID);
        $verifyCPF = array();

        $activity = Activities::find($ID);
        $activity['athletes'] = Athletes::where('activity_id', $activity['id'])->get();
        $activity['event_id'] = Events::where('id', $activity['event_id'])->first();
            foreach ($activity['athletes'] as $athlete) {
                
                $athlete['student_id'] = Student::find($athlete['student_id']);
                // Verificar CPF
                $cont=0;
                foreach ($verifyCPF as $cpf) {
                    if ($cpf == $athlete['student_id']['CPF']) {
                        $cont = $cont+1;
                    }
                }
                if ($cont==0) {
                    array_push($verifyCPF, $athlete['student_id']['CPF']);
                    $athlete['permissao'] = true;
                    
                } else {
                    $athlete['permissao'] = false;
                }
                $athlete['student_id']['CPF'] = $this->mask_CPF($athlete['student_id']['CPF']);
                   $athlete['student_id']['date_birth'] = $this->mask_Data($athlete['student_id']['date_birth']);
                   $athlete['student_id']['phone'] = $this->mask_phone($athlete['student_id']['phone']);
                   $athlete['student_id']['class_id'] = ClassModel::find($athlete['student_id']['class_id']); 
                   
                   if ($athlete['student_id']['class_id'] != null) {
                    $athlete->student_id->class_id->course_id = Course::find( $athlete->student_id->class_id->course_id);     
                   }
                
            }

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Gerou o relatório de presença dos alunos da atividade cujo id é: '.$activity['id']
        );
        Log::create($dataLog);

        $pdf = DomPDF::loadView('report.passengersActivity',compact('activity'));
        return $pdf->download('documento.pdf');
        // return view('report.passengers', compact('event'));
    }


    //mascara CPF
    public function mask_CPF($CPF)
    {
        return substr($CPF, 0, 3).'.'.substr($CPF, 3, 3).'.'.substr($CPF, 6, 3).'-'.substr($CPF, 9, 2);
    }

    //mascara data de nascimento
    public function mask_Data($DATE)
    {
        return substr($DATE, 0, 2).'/'.substr($DATE, 2, 2).'/'.substr($DATE, 4, 4);
    }

    //mascara telefone
    public function mask_phone($PHONE)
    {
        if (strlen($PHONE) == 11) {
            return '('.substr($PHONE, 0, 2).')'.substr($PHONE, 2, 5).'-'.substr($PHONE, 7, 11);
        } else {
            return '('.substr($PHONE, 0, 2).')'.substr($PHONE, 2, 4).'-'.substr($PHONE, 6, 10);
        }
    }
}
