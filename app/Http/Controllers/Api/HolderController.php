<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HolderController extends Controller
{
    //Cadastrar novo Bolsista
    public function setHolder(Request $request)
    {
        # code...
    }

    //Editar um Bolsista cadastrado
    public function updateHolder(Request $request)
    {
        # code...
    }

    //Buscar todos os Bolsistas cadastrados
    public function getHolders()
    {
        # code...
    }

    //Buscar Bolsista pelo ID
    public function getHolder($ID)
    {
        # code...
    }

    //Excluir um Bolsista cadastrado
    public function deleteHolder($ID)
    {
        # code...
    }
}
