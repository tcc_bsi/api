<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Imagick;
use Image;
use Spatie\PdfToImage\Pdf;
use App\Documents;
use Intervention\Image\ImageManagerStatic as ImageImagick;
use App\Log;

class DocumentsController extends Controller
{
    //Cadastrar novos documentos
    public function setDocuments(Request $request)
    {
        $data = $request->all();
        $document = json_decode($data['value'], true);
        if ($request->file('img') != null) {
            
             $path = $request->file('img')->store('documentStudent','local');
        }
        else {
            $path = null; 
        }
        $document['archive'] = $path;
        $document = Documents::create($document);

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a o cadastro de um documento do aluno cujo ID é: '.$document['student_id']
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true,
            'data' => $document
        ]);
    }

    //Listar os documentos cadastrados pelo ID
    public function getDocuments($ID)
    {
        $docs = Documents::where('student_id',$ID)->get();
        foreach ($docs as $var) {
            if ($var['type'] == 'IMAGE') {
                $var['archive'] = $this->getImg($var['id']);
            }
            else if ($var['type'] == 'PDF') {
                $var['archive'] = $this->pdfToPng($var);
            }
        }
        return $docs;
    }

    //Listar somente um documento a partir do ID
    public function getDocument()
    {
        # code...
    }

    //Alterar dados do documento cadastrado a partir do ID
    public function updateDocument(Request $request)
    {
        $data = $request->all();
        $docsValue = json_decode($data['value'], true);
        $docsBase = Documents::find($docsValue['id']);
        if ($request->file('img') != null) {
            Storage::disk('local')->delete($docsBase['archive']);             //deletar antiga imagem
            $path = $request->file('img')->store('documentStudent','local');       //salvando nova imagem
            $docsValue['archive'] = $path;
        } else {
            $docsValue['archive'] = $docsBase['archive'];
        }
        $docsBase->update($docsValue);

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a alteração do documento (id: '.$docsBase['id'].') do aluno cujo ID é: '.$docsBase['student_id']
        );
        Log::create($dataLog);

        return $docsBase;
    }

    //Excluir um documento cadastrado
    public function deleteDocument($ID)
    {
        $document = Documents::find($ID);
        if ($document['archive'] != "") {
            Storage::disk('local')->delete($document['archive']);
        }
        $result = $document->delete();


        if ($result) {
            return response()->json([
                'success' => true
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //Realizar Download do documento selecionado
    public function getDocumentDownload($ID){
        $documents = Documents::find($ID);
        if ($documents['archive'] == "") {
            return response()->json(null);
        }
        if ($documents['type'] == "IMAGE") {
            $storageImage = Storage::disk('local')->get($documents['archive']);
            $image = Image::make($storageImage);
            $image->encode('png');
            $dataLog = array(
                'user_id' => Auth::id(),
                'action' => 'Realizou o download do documento (id: '.$documents['id'].') do aluno cujo ID é: '.$documents['student_id']
            );
            return $image;
        }
        else {
            $path = storage_path('app/'.$documents['archive']);

            $dataLog = array(
                'user_id' => Auth::id(),
                'action' => 'Realizou o download do documento (id: '.$documents['id'].') do aluno cujo ID é: '.$documents['student_id']
            );
            Log::create($dataLog);
            return response()->download($path);
        }
    }

    //Pegar imagens do diretório de documentos
    public function getImg($ID)
    {
        $nameImg = Documents::find($ID);
        if ( $nameImg['archive'] == "") {
            $path = "documentStudent/default.jpg";
        } else {
            $path = $nameImg['archive'];
        }
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(300, 200);
        $image->encode('png');
        return base64_encode($image);
    }
    
    //Transformar PDF em Image para para apresentar nos cards do front-end
    public function pdfToPng($document)
    {
        if ( $document['archive'] == "") {    
            $path = "documentStudent/default.jpg";
            $storageImage = Storage::disk('local')->get($path);
            $image = Image::make($storageImage);
            $image->resize(300, 200);
            $image->encode('png');
            return base64_encode($image);
            return base64_encode($path);
        } else {
            $path = $document['archive'];
        }
        $imagePDF = new Imagick(storage_path().'/app/'.$path.'[0]');
        $imagePDF->setImageFormat('png');
        $imagePDF->cropImage(600, 400, 0, 0); 
        return base64_encode($imagePDF);
    }
}
