<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;
use App\ClassModel;
use Illuminate\Support\Facades\Storage;
use Image;

class ClassCourseController extends Controller
{

    //Cadastrar nova classe

    public function setClass(Request $request){
        $input = $request->all();

        $dataCourse = Course::find($input['course_id']);
        $dataClass = new ClassModel($input);

        $dataClass->course()->associate($dataCourse);
        $result = $dataClass->save();

        if ($result) {
            return  $this->getClass($input['course_id']);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //Cadastrar novo curso

    public function setCourse(Request $request)
    {
         $data = $request->all();
        $course = json_decode($data['value'], true);
        $dataCourse = new Course($course);
        if ($request->file('img') != null) {
            $path = $request->file('img')->store('course_img','local');
            $dataCourse['image'] = $path;
        }
        $result = $dataCourse->save();

        if ($result) {
            return response()->json([
                'success' => true,
                'class' => $dataCourse
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //Listar todos os cursos cadastrados

    public function getCourses()
    {
        return response()->json([
            'data' => Course::all()
        ]);
    }

    //Listar todos as turmas cadastrados

    public function getAllClass()
    {
        return response()->json([
            'data' => ClassModel::all()
        ]);
    }

    //Listar classe/turma pelo ID do curso

    public function getClass($ID)
    {
        return response()->json([
            'data' => ClassModel::where('course_id',$ID)->get()
        ]);
    }

    //Listar cursos e turmas em um só retorno
    public function getClassCourses()
    {
        $courses = Course::all();
        foreach ($courses as $var) {
            $var['class'] = ClassModel::where('course_id',$var['id'])->get();
            $var['image'] = $this->getImg($var['id']);
        }
        // $class = $course->classModel();
        return response()->json([
            'success' => true,
            'data' => $courses
        ]);
    }

    //Atualizar dados do curso

    public function updateCourse(Request $request)
    {
        
        $data = $request->all();
        $courseValue = json_decode($data['value'], true);
        $courseBase = Course::find($courseValue['id']);
        if ($request->file('img') != null) {
            Storage::disk('local')->delete($courseBase['image']);             //deletar antiga imagem
            $path = $request->file('img')->store('course_img','local');       //salvando nova imagem
            $courseValue['image'] = $path;
        } else {
            $courseValue['image'] = $courseBase['image'];
        }

        $courseBase->update($courseValue);

        return $courseBase;
    }

    //Atualizar dados do Turma

    public function updateClass(Request $request)
    {
        $class = ClassModel::find($request['id']);
        $course_id = $request['course_id'];
        $class->update($request->all());
        return $this->getClass($course_id);
    }

    //Deletar classe/turma de acordo com o ID

    public function deleteClass($ID)
    {
        $class = ClassModel::find($ID);
        $course_id = $class['course_id'];
        $result = $class->delete();

        if ($result) {
            return $this->getClass($course_id);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //Deletar curso de acordo com o ID

    public function deleteCourse($ID)
    {
        $course = Course::find($ID);
        Storage::disk('local')->delete($course['image']); 
        $classCourse = ClassModel::where('course_id', $course['id'])->get();
        foreach ($classCourse as $class) {
            $this->deleteClass($class['id']);
        }
        $result = $course->delete();
        
        if ($result) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //Buscar imagem da modalidade a partir do nome do arquivo

    public function getImg($ID)
    {
        
        $nameImg = Course::find($ID);
        if ( $nameImg['image'] == "") {
            $path = "course_img/default.jpg";
        } else {
            $path = $nameImg['image'];
        }
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}
