<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Image;
use App\Athletes;
use App\Student;
use App\Log;

class AthletesController extends Controller
{
    //Anexar atletas em uma modalidade ou atividade
    public function setAthletes(Request $request)
    {
        $athletes = $request->all();
        foreach ($athletes as $athlete) {
            $athlete = Athletes::create($athlete);
            $dataLog = array(
                'user_id' => Auth::id(),
                'action' => 'Realizou a o cadastro do aluno (id:'.$athlete['student_id'].') na atividade cujo ID é: '.$athlete['activity_id']
            );
            Log::create($dataLog);
        }
        return response()->json([
            'success' => true,
            'data' => $athletes
        ]);
    }

    //Buscar atletas todos os atletas que estão cadastrados em pelo menos uma atividade
    public function getAllAthletes()
    {
        $athletes = Athletes::all();
        return response()->json([
            'data' => $athletes
        ]);
    }

    //buscar atletas cadastrados para uma atividade pelo ID da atividade
    public function getAthletes($ID)
    {
        $athletes = Athletes::where('activity_id', $ID)->get();
        foreach ($athletes as $athlete) {
            $id = $athlete['student_id'];
            $student = Student::find($id);
            $student['image'] = $this->getImg($student['id']);
            $athlete['student_id'] = $student;
        }
        
        return response()->json([
            'data' => $athletes
        ]);
    }

    //Excluir atleta cadastrado para em uma determinada modalidade
    public function deleteAthlete($ID)
    {
        $athlete = Athletes::find($ID);
        $result = $athlete->delete();
        if ($result) {

            $dataLog = array(
                'user_id' => Auth::id(),
                'action' => 'Realizou a remoção do aluno (id:'.$athlete['student_id'].') na atividade cujo ID é: '.$athlete['activity_id']
            );
            Log::create($dataLog);

            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

     //Buscar imagem do estudante a partir do nome do arquivo

    public function getImg($ID)
    {
        $nameImg = Student::find($ID);
        if ( $nameImg['image'] == "") {
            $path = "student_img/default.jpg";
        } else {
            $path = $nameImg['image'];
        }
        
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}
