<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use App\Log;

class AdminController extends Controller
{
    //Cadastrar novo Administrador
    public function setAdmin(Request $request)
    {
        # code...
    }

    //Editar um Administrador cadastrado
    public function updateAdmin(Request $request)
    {
        $adminRequest = $request->all();
        $adminData = Admin::find($adminRequest['id']);
        $adminRequest['image'] = $adminData['image'];
        
        $adminData->update($adminRequest);

         $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Alterou seus dados de cadastro '
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true
        ]);
    }

    //Buscar todos os Administradores cadastrados
    public function getAdmins()
    {
        # code...
    }

    //Buscar Administrador pelo ID
    public function getAdmin($ID)
    {
        # code...
    }

    //Excluir um Administrador cadastrado
    public function deleteAdmin($ID)
    {
        # code...
    }
}
