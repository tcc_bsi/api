<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Student;
use App\Address;
use App\Responsible;
use App\Bank;
use App\ClassModel;
use App\Athletes;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Log;

class StudentController extends Controller
{

    // Cadastrando novo estudante e suas dependências

    public function setStudent(Request $request){

        $req = $request->all();

        $input = json_decode($req['value'], true);

        if ($request->file('img') != null) {
            $path = $request->file('img')->store('student_img', 'local');
        } else {
            $path = null;
        }
        $input['student']['image'] = $path;
        
        $address = $input['student']['address'];
        $student = $input['student'];
        $responsible = $input['student']['responsible'];
        $bank = $input['student']['bank'];
        $class_id = $input['student']['class_model']['id'];

        $dataStudent = new Student($student);

        // Salvando e Associando o endereço com estudante
        $dataAddress = Address::create($address);
        $dataStudent->address()->associate($dataAddress);

        // Salvando e Associando o banco com estudante
        $dataBank = Bank::create($bank);
        $dataStudent->bank()->associate($dataBank);

        // Salvando e Associando a classe com estudante
        $dataClass = ClassModel::find($class_id);
        $dataStudent->classModel()->associate($dataClass);
        //condição tratada no front-end: se o estudante for maior de idade não precisa de responsávelq
        if ($responsible != null) {
            // Salvando e Associando o responsável com estudante
            $dataResponsible = Responsible::create($responsible);   
            $dataStudent->responsible()->associate($dataResponsible);
        }
        
        //salvando dados do estudante 
        $result = $dataStudent->save();

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a o cadastro do aluno cujo ID é: '.$dataStudent['id']
        );
        Log::create($dataLog);

        if ($result) {
            return response()->json([
                'success' => true,
                'student' => $dataStudent
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    // Listar todos os estudantes cadastrados 

    public function getStudents(){

        $student = Student::all();
        foreach ($student as $value) {
            $value['image'] = $this->getImg($value['id']);
        }

        return response()->json([
            'data' => $student
        ]);
    }

    // Listar todos os que ainda nao foram adicionados em uma determinada atividade

    public function getStudentsByActivity($ID){

        $student = Student::all();
        $athletes = $this->getAthletes($ID);
        foreach ($student as $stu) {
            $stu['image'] = $this->getImg($stu['id']);
            $stu['visible'] = true;
            foreach ($athletes as $atl) {
                if ($stu['id'] == $atl['student_id']['id']) {
                    $stu['visible'] = false;
                }
            }
        }

        return response()->json([
            'data' => $student
        ]);
    }

    //buscar atletas cadastrados para uma atividade pelo ID da atividade
    public function getAthletes($ID)
    {
        $athletes = Athletes::where('activity_id', $ID)->get();
        foreach ($athletes as $athlete) {
            $id = $athlete['student_id'];
            $student = Student::find($id);
            $student['image'] = $this->getImg($student['id']);
            $athlete['student_id'] = $student;
        }
        return $athletes;
    }

    //Listar um estudante cadastrado, pelo ID

    public function getStudent($ID){
        $student = Student::find($ID);
        $address = $student->address;
        $responsible = $student->responsible;
        $bank = $student->bank;
        
        $class = $student->classModel;
        if ($class != null) {
            $course = $class->course;
        }
        
        $student['image'] = $this->getImg($ID);

        return response()->json([
            'student' => $student,
        ]);
    }

    //Atualizar dados do estudante cadastrado 

    public function updateStudent(Request $request){
        $req = $request->all();
        $user = $request->user();

        $data = json_decode($req['value'], true);
        
        // $data = $request->all();

        $student = Student::find($data['student']['id']);

        if ($request->file('img') != null) {
            Storage::disk('local')->delete($student['image']);             //deletar antiga imagem
            $path = $request->file('img')->store('student_img','local');       //salvando nova imagem
            $data['student']['image'] = $path;
        } else {
            $data['student']['image'] = $student['image'];
        }

        if ($data['student']['address']['id'] != null) {
            $student->address->update($data['student']['address']);
        }
        
        if ($data['student']['responsible']['id'] != null) {
            $student->responsible->update($data['student']['responsible']);
        }
        
        if ($data['student']['bank']['id'] != null) {
            $student->bank->update($data['student']['bank']);
        }

        // if ($data['student']['class_model']['id'] != null) {
        //     $student->classModel()->dissociate();
        //     $student->classModel()->associate($data['student']['class_model']['id']);
        // }

        $data['student']['class_id'] = $data['student']['class_model']['id'];
        
        
        $student->update($data['student']);
        
        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a alteração do aluno cujo ID é: '.$data['student']['id']
        );
        Log::create($dataLog);

        return response()->json([
            'student' => $student,
        ]);
    }

    //Excluir dados do estudante cadastrado

    public function deleteStudent($ID){
        
        $student = Student::find($ID);
        $address_id = $student['address_id'];
        $responsible_id = $student['responsible_id'];
        $bank_id = $student['bank_id'];
        $student->address()->dissociate();
        $student->responsible()->dissociate();
        $student->bank()->dissociate();
        $student->classModel()->dissociate();
        $student->save();
        
        $address = Address::find($address_id);
        $address->delete();
        $responsible = Responsible::find($responsible_id);
        $responsible->delete();
        $bank = Bank::find($bank_id);
        $bank->delete();
        $result = $student->delete();
        if ($result) {
            return response()->json([
                'success' => true
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

     //Buscar imagem do estudante a partir do nome do arquivo

    public function getImg($ID)
    {
        $nameImg = Student::find($ID);
        if ( $nameImg['image'] == "") {
            $path = "student_img/default.jpg";
        } else {
            $path = $nameImg['image'];
        }
        
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}   
