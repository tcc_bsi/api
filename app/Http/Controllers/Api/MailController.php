<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\User;

class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        
        $func = json_decode(json_encode($this->dataMail($request)));
        $mail = $func->original;
        $contentMail;
        $contentMail['recipient'] = $mail->recipient->nickname;
        $contentMail['sender'] = $mail->sender->nickname;
        $contentMail['content'] = $mail->content; 
        $contentMail['subject'] = $mail->subject;
        Mail::send('mail.pages.PasswordGenerated',compact('contentMail'), function($m) use ($mail){
            $m->from($mail->sender->email, $mail->sender->nickname);
            $m->to($mail->recipient->email);
            $m->subject($mail->subject);

        });
        return response()->json([
            'success' => true,
            'msg' => 'mensagem enviada com sucesso'
        ]);
    }

    public function dataMail(Request $request)
    {
        $data = $request->all();
        return response()->json([
            'sender' => User::where('id', $data['sender'])->first(),
            'recipient' => User::where('id', $data['recipient'])->first(),
            'content' => $data['content'],
            'subject' => $data['subject']
        ]);
    }
    

}
