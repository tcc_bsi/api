<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Image;
use App\Activities;
use App\Modality;
use App\Log;

class ActivityController extends Controller
{
    //Cadastrar nova Atividade
    public function setActivity(Request $request)
    {
        $data = $request->all();
        $activity = json_decode($data['value'], true);
        $activity = Activities::create($activity);

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a o cadastro da atividade cujo ID é: '.$activity['id']
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true,
            'data' => $activity
        ]);
    }

    //Editar uma Atividade cadastrado
    public function updateActivity(Request $request)
    {
        $data = $request->all();
        $activityValue = json_decode($data['value'], true);
        
        $activityBase = Activities::find($activityValue['id']);
        
        $activityBase->update($activityValue);

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Realizou a alteração da atividade cujo ID é: '.$activityBase['id']
        );
        Log::create($dataLog);

        // $activityBase['image'] = $this->getImg($activityBase['modality_id']);
        return $activityBase;
    }

    //Buscar todos as Atividade cadastrados pelo ID do evento relacionado
    public function getActivities($ID)
    {
        $activity = Activities::where('event_id', $ID)->get();
        foreach ($activity as $value) {
            $value['image'] = $this->getImg($value['modality_id']);
        }
        return response()->json([
            'data' => $activity
        ]);
    }

    //Buscar Atividade pelo ID
    public function getActivity($ID)
    {
        # code...
    }

    //Excluir uma Atividade cadastrado
    public function deleteActivity($ID)
    {
        $activity = Activities::find($ID);
        if ($activity['image'] != "") {
            Storage::disk('local')->delete($activity['image']);
        }
        $result = $activity->delete();
        if ($result) {
            return response()->json([
                'success' => true
            ]);
        }
        else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    //BUSCAR IMAGEM DA ATIVIDADE A PARTIR DO ID

    public function getImg($ID)
    {
        // $nameImg = Activities::find($ID);
        $modality = Modality::find($ID);

        if ($modality['image'] == "") {
            $path = "modality_img/default.jpg";
        } else {
            $path = $modality['image'];
        }
        
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}
