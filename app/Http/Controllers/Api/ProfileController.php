<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Admin;
use App\Manager;
use App\Holder;
use App\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Log;

class ProfileController extends Controller
{
    public function getProfile(Request $request){
        $user = $request->user();
        
        if ($user['type_user'] == 'ADMINISTRADOR') {
                $user['type_user_data'] = Admin::where('user_id', $user['id'])->first();
                $user['type_user_data']['image'] = $this->getImage($user['type_user_data']['image']);
        } else if ($user['type_user'] == 'PROFESSOR') {
            $user['type_user_data'] = Manager::where('user_id', $user['id'])->first();
            // dd($user['type_user_data']['image']);
            $image = $user->type_user_data;
            $image['image'] = $this->getImage($image['image']);
            $user->type_user_data = $image;
        }else if ($user['type_user'] == 'BOLSISTA') {
            $holder = Holder::where('user_id', $user['id'])->first();
            $student = Student::find($holder['student_id']);
            $user['type_user_data'] = $student;
            $image = $user->type_user_data;
            $image['image'] = $this->getImage($image['image']);
            $user->type_user_data = $image;
            // $user['type_user_data']['image'] = $this->getImage($user['type_user_data']['image']);
        } else {
            $user['type_user_data'] = null;
        }
        return $user;
    }

    public function setProfile(){

    }

    public function updateProfile(Request $request){
        // dd("VEIO");
        $userRequest = $request->all();
        $userData = User::find($userRequest['id']);
        
        $userData->update($userRequest);

         $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Alterou seus dados de cadastro '
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true
        ]);
    }

    public function deleteProfile(){

    }

    public function getImage($path)
    {
        if ($path == "") {
            $path = "userImage/default.jpg";
        } 
        
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}
