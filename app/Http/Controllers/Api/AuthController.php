<?php

namespace App\Http\Controllers\Api;


use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Admin;
use App\Manager;
use App\Holder;
use App\Student;
use Validator;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Support\Facades\Auth;
use App\Log;

class AuthController extends Controller
{

    //Cadastrar novo usuário

    public function setUser(Request $request){
        $input = $request->all();
        $userLogged = $request->user();
        $passRandom = str_random(8);
        $input['password'] = Hash::make($passRandom);
        $input['user_id'] = $userLogged['id'];
        $user = User::create($input);
        if ($user['type_user'] == 'BOLSISTA') {
            $holder = Holder::create([
                'student_id' => $input['student_id'],
                'user_id' => $user['id'],
            ]);
        }

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Cadastrou um novo usuário cujo id é: '.$user['id']
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true,
            'data' => $user,
            'passRandom' => $passRandom
        ]);
    }

    // Editar usuário

    public function updateUser(Request $request){
        $input = $request->all();
        $user = $request->user();
        $input['id'] = $user['id'];
        // $user = User::find($request['id']);
        if ($input['password'] != null) {
            $input['password'] = Hash::make($input['password']);
        }
        $user->update($input);

        $dataLog = array(
            'user_id' => Auth::id(),
            'action' => 'Alterou seus dados de cadastro '
        );
        Log::create($dataLog);

        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }

    //autorização de acesso

    public function login(Request $request){
        $data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );
        if (Auth::attempt($data)) {
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->accessToken;
            if ($user['type_user'] == 'ADMINISTRADOR') {
                $user['type_user_id'] = Admin::where('user_id', $user['id'])->first();
            } else if ($user['type_user'] == 'PROFESSOR') {
                $user['type_user_id'] = Manager::where('user_id', $user['id'])->first();
            }else if ($user['type_user'] == 'BOLSISTA') {
                $user['type_user_id'] = Holder::where('user_id', $user['id'])->first();
            } else {
                $user['type_user_id'] = null;
            }
            return response()->json([
                'success' => 'Login realizado com sucesso!',
                'token' => $token,
                'user' => $user
            ], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }  
    }

    //Listar Usuários
    public function getUsers(Request $request){
        // $users = User::all();
        $userRequest = $request->user();
        if ($userRequest['type_user'] == 'ADMINISTRADOR') {
            $users = User::all();
        } else if ($userRequest['type_user'] == 'PROFESSOR') {
            $users = User::where('type_user', 'BOLSISTA')->get();
        }
        foreach ($users as $user) {
            $user['user_id'] = User::find($user['user_id']);
            $user['image'] = $this->getImg($user['id']);
        }
        return response()->json([
            'data' => $users
        ]);
    }

    //Fechar Autorização de Acesso.

    public function logout(Request $request){
        $token = $request->user()->token();
        $token->revoke();
    
        $response = 'Sucesso!';
        return response()->json([
            'msg' => $response
        ], 200);
    
    }

     public function getImg($ID)
    {
        $path = null;
        $user = User::find($ID);
        if ($user['type_user'] == 'PROFESSOR') {
            $manager = Manager::where('user_id', $user['id'])->first();
            $path = $manager['image'];
        } else if ($user['type_user'] == 'BOLSISTA') {
            $holder = Holder::where('user_id', $user['id'])->first();
            $student = Student::find($holder['student_id']);
            $path = $student['image'];
        }

        if ( $path == "") {
            $path = "student_img/default.jpg";
        }
        
        $storageImage = Storage::disk('local')->get($path);
        $image = Image::make($storageImage);
        $image->resize(200, 120);
        $image->encode('png');
        return base64_encode($image);
    }
}
