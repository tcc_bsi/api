<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    // Tabela usada nesta model 

    protected $table = 'bank';

    // Informações da tabela 'student' liberados para inserção

    protected $fillable = [
        'bank_name', 'agency', 'account'
    ];
}
