<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
    // Tabela usada nesta model

    protected $table = 'responsible';

    // Informações da tabela 'responsible' liberados para inserção

    protected $fillable = [
        'name', 'RG', 'family', 'phone'
    ];
}
