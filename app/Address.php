<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    // Tabela usada nesta model

    protected $table = 'address';

    // Informações da tabela 'address' liberados para inserção

    protected $fillable = [
        'city', 'CEP', 'street','complement', 'district', 'number'
    ];
}
