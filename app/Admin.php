<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    // Tabela usada nesta model

    protected $table = 'admin';

    // Informações da tabela 'admin' liberados para inserção

    protected $fillable = [
        'name', 'user_id', 'image'
    ];
}
