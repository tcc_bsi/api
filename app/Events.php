<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
     // Tabela usada nesta model

    protected $table = 'events';

    // Informações da tabela 'events' liberados para inserção

    protected $fillable = [
        'title', 'date_start', 'date_end','city', 'description', 'image', 'CEP', 'type_game'
    ];
}
