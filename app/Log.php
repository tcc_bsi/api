<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    // Tabela usada nesta model

    protected $table = 'log';

    // Informações da tabela 'log' liberados para inserção

    protected $fillable = [
        'action', 'user_id'
    ];
}
