<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
     // Tabela usada nesta model

    protected $table = 'activities';

    // Informações da tabela 'activities' liberados para inserção

    protected $fillable = [
        'title', 'rank', 'total_teams', 'modality_id', 'event_id', 'limit_athletes'
    ];
}
