<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    // Tabela usada nesta model

    protected $table = 'manager';

    // Informações da tabela 'manager' liberados para inserção

    protected $fillable = [
        'first_name', 'last_name', 'CPF','user_id', 'image'
    ];
}
