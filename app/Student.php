<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    // Tabela usada nesta model 

    protected $table = 'student';

    // Informações da tabela 'student' liberados para inserção

    protected $fillable = [
        'first_name', 'last_name', 'matricula','date_birth','sexo', 'CPF', 'phone', 'RG', 'image', 'class_id', 'responsible_id'
    ];

    // Relacionamento 1x1 entre as tabelas 'student' e 'address' 

    public function address()
    {
        return $this->belongsTo('App\Address','address_id');
    }

    // Relacionamento 1x1 entre as tabelas 'student' e 'responsible' 

    public function responsible()
    {
        return $this->belongsTo('App\Responsible','responsible_id');
    }

    // Relacionamento 1x1 entre as tabelas 'student' e 'bank' 

    public function bank()
    {
        return $this->belongsTo('App\Bank','bank_id');
    }

    // Relacionamento 1x1 entre as tabelas 'student' e 'class' 

    public function classModel()
    {
        return $this->belongsTo('App\ClassModel','class_id');
    }
}
