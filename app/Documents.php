<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    //Tabela usada nesta model
    protected $table = 'documents';

    //Campos que autorizados a receber valores da requisição
    protected $fillable = [
        'title', 'archive', 'type', 'student_id'
    ];
}
