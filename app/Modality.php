<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    // Tabela usada nesta model 

    protected $table = 'modality';

    // Informações da tabela 'modality' liberados para inserção

    protected $fillable = [
        'title', 'description', 'modality_id', 'image'
    ];
}
